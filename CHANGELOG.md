##  Changelog
v.1.0.0 大部分动画效果已支持

#目前支持的动画效果
1)	Attension动画："Flash", "Pulse", "RubberBand", "Shake", "Swing", "Wobble", "Bounce", "Tada"
2)	Special动画："RollIn", "RollOut","Landing","TakingOff","DropOut"
3)	Bounce动画："BounceIn", "BounceInDown", "BounceInLeft", "BounceInRight", "BounceInUp"
4)	Fade动画："FadeIn", "FadeInUp", "FadeInDown", "FadeInLeft", "FadeInRight", "FadeOut", "FadeOutDown", "FadeOutLeft", "FadeOutRight", "FadeOutUp"
5)	Slide动画："SlideInLeft", "SlideInRight", "SlideInUp", "SlideInDown", "SlideOutLeft", "SlideOutRight", "SlideOutUp", "SlideOutDown"
6)	Zoom动画："ZoomIn", "ZoomInDown", "ZoomInLeft", "ZoomInRight", "ZoomInUp", "ZoomOut", "ZoomOutDown", "ZoomOutLeft", "ZoomOutRight", "ZoomOutUp"

#目前不支持的动画效果
1)	Attension动画："StandUp", "Wave"
2)	Special动画："Hinge"
3)	Flippers动画："FlipInX", " FlipInY", " FlipOutX", " FlipOutY"
4)	Romote动画："RotateIn", "RotateInDownLeft", "RotateInDownRight", "RotateInUpLeft", "RotateInUpRight", "RotateOut", "RotateOutDownLeft", "RotateOutDownRight", "RotateOutUpLeft", "RotateOutUpRight"

v.1.0.3 一些动画效果已支持
1)	Attension动画："Wave"
2)	Romote动画："RotateIn", "RotateInDownLeft", "RotateInDownRight", "RotateInUpLeft", 
"RotateInUpRight", "RotateOut", "RotateOutDownLeft", "RotateOutDownRight", "RotateOutUpLeft", "RotateOutUpRight"






