# ohos View Animations

### Demo
![ohosViewAnimations](https://gitee.com/openharmony-tpc/ohosViewAnimations/raw/master/screenshot/demo.gif)

# Usage

Solution 1: local har package integration
1. Add the har package to the lib folder.
2. Add the following code to gradle of the entry:
```groovy
implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
```

Solution 2:
```groovy
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:ohosViewAnimations:1.0.4'
```


## Entry Running Requirements
1. Use DevEco Studio and download the SDK.
2. Change the dependencies→classpath version in the build.gradle file to the corresponding version. (that is, the version used in your IDE's new project)


```java
YoYo.with(Techniques.Tada)
    .duration(1000)
    .delay(100)
    .playOn(findComponentById(ResourceTable.Id_edit_area));
```

### Effects
#### Attension
`Flash`, `Pulse`, `RubberBand`, `Shake`, `Swing`, `Wobble`, `Bounce`, `Tada`

#### Special
`RollIn`, `RollOut`,`Landing`,`TakingOff`,`DropOut`

#### Bounce
`BounceIn`, `BounceInDown`, `BounceInLeft`, `BounceInRight`, `BounceInUp`

#### Fade
`FadeIn`, `FadeInUp`, `FadeInDown`, `FadeInLeft`, `FadeInRight`

`FadeOut`, `FadeOutDown`, `FadeOutLeft`, `FadeOutRight`, `FadeOutUp`

#### Slide
`SlideInLeft`, `SlideInRight`, `SlideInUp`, `SlideInDown`

`SlideOutLeft`, `SlideOutRight`, `SlideOutUp`, `SlideOutDown`

#### Zoom
`ZoomIn`, `ZoomInDown`, `ZoomInLeft`, `ZoomInRight`, `ZoomInUp`

`ZoomOut`, `ZoomOutDown`, `ZoomOutLeft`, `ZoomOutRight`, `ZoomOutUp`

Welcome contribute your amazing animation effect. :-D
