/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.daimajia.ohosanimations.library.zooming_entrances;


import com.daimajia.ohosanimations.library.AnimatorValueUtils;
import com.daimajia.ohosanimations.library.BaseViewAnimator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class ZoomInLeftAnimator extends BaseViewAnimator {
    @Override
    public void prepare(Component target) {
        int distance = target.getRight();
        AnimatorValue animator = new AnimatorValue();
        animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                target.setScaleX(AnimatorValueUtils.getAnimatedValue(v, 0.1f, 0.475f, 1));
                target.setScaleY(AnimatorValueUtils.getAnimatedValue(v, 0.1f, 0.475f, 1));
                target.setTranslationX(AnimatorValueUtils.getAnimatedValue(v, -distance, 48, 0));
                target.setAlpha(AnimatorValueUtils.getAnimatedValue(v, 0, 1, 1));
            }
        });
        getAnimatorAgent().runParallel(animator);
    }
}
