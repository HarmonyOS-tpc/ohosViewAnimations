/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daimajia.ohosanimations.library;

/**
 * 估值其类型
 */
public enum Skill {
    QuintEaseOut(com.daimajia.ohosanimations.library.easing.QuintEaseOut.class),
    SineEaseInOut(com.daimajia.ohosanimations.library.easing.SineEaseInOut.class);

    private Class easingMethod;

    Skill(Class clazz) {
        easingMethod = clazz;
    }

    /**
     * 生成BaseEasingMethod
     * @param duration 时间间隔
     * @return BaseEasingMethod
     */
    public BaseEasingMethod getMethod(float duration) {
        try {
            return (BaseEasingMethod) easingMethod.getConstructor(float.class).newInstance(duration);
        } catch (Exception e) {
            return null;
        }
    }
}
