/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daimajia.ohosanimations.library.easing;


import com.daimajia.ohosanimations.library.BaseEasingMethod;

/**
 * 估值器
 */
public class SineEaseInOut extends BaseEasingMethod {
    /**
     * 构造
     * @param duration 时间
     */
    public SineEaseInOut(float duration) {
        super(duration);
    }

    @Override
    public Float calculate(float time, float start, float difference, float duration) {
        return -difference / 2 * ((float) Math.cos(Math.PI * time / duration) - 1) + start;
    }

    /**
     * 获得动画结果
     *
     * @param fraction   动画完成度
     * @param startValue 动画开始位置
     * @param endValue   动画结束位置
     * @return 动画结果
     */
    public final Float estimate(float fraction, Float startValue, Float endValue) {
        float t = mDuration * fraction;
        float b = startValue;
        float c = endValue - startValue;
        float d = mDuration;
        float result = calculate(t, b, c, d);
        return result;
    }
}
