/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.daimajia.ohosanimations.library;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.components.Component;


public abstract class BaseViewAnimator {
    private boolean mStarted = false;

    public static final long DURATION = 1000;

    private AnimatorGroup mAnimatorSet;

    private long mDuration = DURATION;
    private int mRepeatTimes = 0;
    private int mCurveType = Animator.CurveType.INVALID;

    {
        mAnimatorSet = new AnimatorGroup();
    }

    protected abstract void prepare(Component target);

    public BaseViewAnimator setTarget(Component target) {
        reset(target);
        prepare(target);
        return this;
    }

    public void animate() {
        start();
    }

    public void restart() {
        mStarted = false;
        mDuration = DURATION;
        start();
    }

    /**
     * reset the view to default status
     *
     * @param target
     */
    public void reset(Component target) {
        target.setAlpha(1);
        target.setScaleX(1);
        target.setScaleY(1);
        target.setTranslationX(0);
        target.setTranslationY(0);
        target.setRotation(0);
        if (mAnimatorSet != null) {
            mAnimatorSet.clear();
        }
    }

    /**
     * start to animate
     */
    public void start() {
        if (mAnimatorSet != null) {
            mAnimatorSet.setDuration(mDuration);
            mAnimatorSet.setLoopedCount(mRepeatTimes);
            mAnimatorSet.setCurveType(mCurveType);
            mAnimatorSet.start();
            mStarted = true;
        }
    }

    public BaseViewAnimator setDuration(long duration) {
        mDuration = duration;
        return this;
    }

    public BaseViewAnimator setStartDelay(long delay) {
        getAnimatorAgent().setDelay(delay);
        return this;
    }

    public long getStartDelay() {
        return mAnimatorSet.getDelay();
    }

    public BaseViewAnimator addAnimatorListener(Animator.StateChangedListener l) {
        mAnimatorSet.setStateChangedListener(l);
        return this;
    }

    public void cancel() {
        mAnimatorSet.cancel();
    }

    public boolean isRunning() {
        return mAnimatorSet.isRunning();
    }

    public boolean isStarted() {
        return mStarted;
    }

    public void removeAnimatorListener(Animator.StateChangedListener l) {
        l = null;
        mAnimatorSet.setStateChangedListener(l);
    }

    public void removeAllListener() {
        mAnimatorSet.clear();
        mAnimatorSet.setStateChangedListener(null);
    }

    public BaseViewAnimator setCurveType(int curveType) {
        mCurveType = curveType;
        return this;
    }

    public int getCurveType() {
        return mAnimatorSet.getCurveType();
    }

    public long getDuration() {
        return mDuration;
    }

    public AnimatorGroup getAnimatorAgent() {
        return mAnimatorSet;
    }

    public BaseViewAnimator setRepeatTimes(int repeatTimes) {
        mRepeatTimes = repeatTimes;
        return this;
    }
}
