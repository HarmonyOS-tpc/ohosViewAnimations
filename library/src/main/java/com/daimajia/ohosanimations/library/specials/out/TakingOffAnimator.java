package com.daimajia.ohosanimations.library.specials.out;


import com.daimajia.ohosanimations.library.BaseViewAnimator;
import com.daimajia.ohosanimations.library.Glider;
import com.daimajia.ohosanimations.library.easing.QuintEaseOut;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class TakingOffAnimator extends BaseViewAnimator {
    @Override
    protected void prepare(Component target) {
        long duration = getDuration();
        AnimatorValue animator = new AnimatorValue();
        Glider.glide(animator, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float result = new QuintEaseOut(duration).estimate(v, 1f, 1.5f);
                target.setScaleX(result);
                target.setScaleY(result);
                target.setAlpha(new QuintEaseOut(duration).estimate(v, 1f, 0f));
            }
        });
        getAnimatorAgent().runParallel(animator);
    }
}
