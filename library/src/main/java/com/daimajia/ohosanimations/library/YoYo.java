/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.daimajia.ohosanimations.library;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

public class YoYo {
    private static final long DURATION = BaseViewAnimator.DURATION;
    private static final long NO_DELAY = 0;
    public static final int INFINITE = -1;
    public static final float CENTER_PIVOT = Float.MAX_VALUE;

    private BaseViewAnimator animator;
    private long duration;
    private long delay;
    private boolean repeat;
    private int repeatTimes;
    private int curveType;
    private float pivotX, pivotY;
    private List<Animator.StateChangedListener> callbacks;
    private Component target;

    private YoYo(AnimationComposer animationComposer) {
        animator = animationComposer.animator;
        duration = animationComposer.duration;
        delay = animationComposer.delay;
        repeat = animationComposer.repeat;
        repeatTimes = animationComposer.repeatTimes;
        curveType = animationComposer.curveType;
        pivotX = animationComposer.pivotX;
        pivotY = animationComposer.pivotY;
        callbacks = animationComposer.callbacks;
        target = animationComposer.target;
    }

    public static AnimationComposer with(Techniques techniques) {
        return new AnimationComposer(techniques);
    }

    public static AnimationComposer with(BaseViewAnimator animator) {
        return new AnimationComposer(animator);
    }

    public interface AnimatorCallback {
        void call(Animator animator);
    }

    private static class EmptyAnimatorListener implements Animator.StateChangedListener {
        @Override
        public void onStart(Animator animator) {}

        @Override
        public void onStop(Animator animator) {}

        @Override
        public void onCancel(Animator animator) {}

        @Override
        public void onEnd(Animator animator) {}

        @Override
        public void onPause(Animator animator) {}

        @Override
        public void onResume(Animator animator) {}
    }

    public static final class AnimationComposer {
        private List<Animator.StateChangedListener> callbacks = new ArrayList<>();

        private BaseViewAnimator animator;
        private long duration = DURATION;

        private long delay = NO_DELAY;
        private boolean repeat = false;
        private int repeatTimes = 0;
        private float pivotX = YoYo.CENTER_PIVOT, pivotY = YoYo.CENTER_PIVOT;
        private int curveType;
        private Component target;

        private AnimationComposer(Techniques techniques) {
            this.animator = techniques.getAnimator();
        }

        private AnimationComposer(BaseViewAnimator animator) {
            this.animator = animator;
        }

        public AnimationComposer duration(long duration) {
            this.duration = duration;
            return this;
        }

        public AnimationComposer delay(long delay) {
            this.delay = delay;
            return this;
        }

        public AnimationComposer curveType(int curveType) {
            this.curveType = curveType;
            return this;
        }

        public AnimationComposer pivot(float pivotX, float pivotY) {
            this.pivotX = pivotX;
            this.pivotY = pivotY;
            return this;
        }

        public AnimationComposer pivotX(float pivotX) {
            this.pivotX = pivotX;
            return this;
        }

        public AnimationComposer pivotY(float pivotY) {
            this.pivotY = pivotY;
            return this;
        }

        public AnimationComposer repeat(int times) {
            if (times < INFINITE) {
                throw new RuntimeException("Can not be less than -1, -1 is infinite loop");
            }
            repeat = times != 0;
            repeatTimes = times;
            return this;
        }

        public AnimationComposer withListener(Animator.StateChangedListener listener) {
            callbacks.add(listener);
            return this;
        }

        public AnimationComposer onStart(final AnimatorCallback callback) {
            callbacks.add(
                    new EmptyAnimatorListener() {
                        @Override
                        public void onStart(Animator animator) {
                            super.onStart(animator);
                        }
                    });
            return this;
        }

        public AnimationComposer onEnd(final AnimatorCallback callback) {
            callbacks.add(
                    new EmptyAnimatorListener() {
                        @Override
                        public void onEnd(Animator animator) {
                            super.onEnd(animator);
                        }
                    });
            return this;
        }

        public AnimationComposer onCancel(final AnimatorCallback callback) {
            callbacks.add(
                    new EmptyAnimatorListener() {
                        @Override
                        public void onCancel(Animator animator) {
                            super.onCancel(animator);
                        }
                    });
            return this;
        }

        public YoYoString playOn(Component target) {
            this.target = target;
            return new YoYoString(new YoYo(this).play(), this.target);
        }
    }

    /**
     * YoYo string, you can use this string to control your YoYo.
     */
    public static final class YoYoString {
        private BaseViewAnimator animator;
        private Component target;

        private YoYoString(BaseViewAnimator animator, Component target) {
            this.target = target;
            this.animator = animator;
        }

        public boolean isStarted() {
            return animator.isStarted();
        }

        public boolean isRunning() {
            return animator.isRunning();
        }

        public void stop() {
            stop(true);
        }

        public void stop(boolean reset) {
            animator.cancel();
            if (reset){
                animator.reset(target);
            }
        }
    }

    private BaseViewAnimator play() {

        if (pivotX == YoYo.CENTER_PIVOT) {
            target.setPivotX(target.getWidth() / 2.0f);
        } else {
            target.setPivotX(pivotX);
        }
        if (pivotY == YoYo.CENTER_PIVOT) {
            target.setPivotY(target.getHeight() / 2.0f);
        } else {
            target.setPivotY(pivotY);
        }

        animator.setDuration(duration).setCurveType(curveType).setRepeatTimes(repeatTimes).setStartDelay(delay);

        if (callbacks.size() > 0) {
            for (Animator.StateChangedListener callback : callbacks) {
                animator.addAnimatorListener(callback);
            }
        }
        animator.setTarget(target);
        animator.animate();
        return animator;
    }
}
