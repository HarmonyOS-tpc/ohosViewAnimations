/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daimajia.ohosanimations.library;

import ohos.agp.animation.AnimatorValue;

/**
 * 给动画设置估值器
 */
public class Glider {
    /**
     * 根据传入参数生成动画
     * @param skill 自定义估值器类型
     * @param duration 时间
     * @param animator 动画
     * @param valueUpdateListener 值监听器
     * @param listeners 回调
     * @return 动画
     */
    public static AnimatorValue glide(
            Skill skill,
            float duration,
            AnimatorValue animator,
            AnimatorValue.ValueUpdateListener valueUpdateListener,
            BaseEasingMethod.EasingListener... listeners) {
        BaseEasingMethod method = skill.getMethod(duration);

        if (listeners != null) {
            method.addEasingListeners(listeners);
        }

        animator.setValueUpdateListener(valueUpdateListener);
        return animator;
    }

    public static AnimatorValue glide(AnimatorValue animator, AnimatorValue.ValueUpdateListener valueUpdateListener) {
        animator.setValueUpdateListener(valueUpdateListener);
        return animator;
    }
}
