package com.daimajia.ohosanimations.library.specials.in;


import com.daimajia.ohosanimations.library.AnimatorValueUtils;
import com.daimajia.ohosanimations.library.BaseViewAnimator;
import com.daimajia.ohosanimations.library.Glider;
import com.daimajia.ohosanimations.library.easing.BounceEaseOut;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class DropOutAnimator extends BaseViewAnimator {
    @Override
    protected void prepare(Component target) {
        int distance = target.getTop() + target.getHeight();
        long duration = getDuration();
        AnimatorValue animator = new AnimatorValue();
        Glider.glide(animator, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float start = -distance;
                float end = 0;
                float result = new BounceEaseOut(duration).estimate(v, start, end);
                target.setAlpha(AnimatorValueUtils.getAnimatedValue(v, 0, 1));
                target.setTranslationY(result);

            }
        });
        getAnimatorAgent().runParallel(animator);
    }
}
