/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daimajia.ohosanimations;

import com.daimajia.ohosanimations.library.Techniques;

import ohos.agp.components.*;
import ohos.app.Context;

/**
 * 适配器
 */
public class EffectAdapter extends BaseItemProvider {
    private Context mContext;

    public EffectAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return Techniques.values().length;
    }

    @Override
    public Object getItem(int position) {
        return Techniques.values()[position].getAnimator();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component itemComponent = component;
        ViewHolder viewHolder;
        if (itemComponent == null) {
            itemComponent =
                    LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item, componentContainer, false);
        }
        viewHolder = new ViewHolder();
        viewHolder.text = (Text) itemComponent.findComponentById(ResourceTable.Id_list_item_text);
        Object object = getItem(position);
        int start = object.getClass().getName().lastIndexOf(".") + 1;
        String name = object.getClass().getName().substring(start);
        viewHolder.text.setText(name);
        itemComponent.setTag(Techniques.values()[position]);
        return itemComponent;
    }

    private static class ViewHolder {
        Text text;
    }
}
