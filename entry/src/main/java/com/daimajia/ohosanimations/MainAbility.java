/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daimajia.ohosanimations;

import com.daimajia.ohosanimations.library.Techniques;
import com.daimajia.ohosanimations.library.YoYo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

/**
 * 主页
 */
public class MainAbility extends Ability {
    private ListContainer mListView;
    private Text mTarget;
    private YoYo.YoYoString rope;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_my);
        mTarget = (Text) findComponentById(ResourceTable.Id_text);
        mListView = (ListContainer) findComponentById(ResourceTable.Id_list_items);
        EffectAdapter adapter = new EffectAdapter(this);
        mListView.setItemProvider(adapter);
        mListView.setItemClickedListener(
                (listContainer, component, i, l) -> {
                    if (rope != null) {
                        rope.stop(true);
                    }
                    Techniques technique = (Techniques) component.getTag();
                    rope = YoYo.with(technique).duration(2000).withListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {

                        }

                        @Override
                        public void onStop(Animator animator) {

                        }

                        @Override
                        public void onCancel(Animator animator) {
                            new ToastDialog(getContext()).setText("canceled previous animation").show();
                        }

                        @Override
                        public void onEnd(Animator animator) {

                        }

                        @Override
                        public void onPause(Animator animator) {

                        }

                        @Override
                        public void onResume(Animator animator) {

                        }
                    }).playOn(mTarget);
                });
        mTarget.setClickedListener(component -> {
            if (rope != null) {
                rope.stop(true);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            rope = YoYo.with(Techniques.FadeIn).duration(1000).playOn(mTarget);// after start,just click mTarget view, rope is not init
        }
    }

}
